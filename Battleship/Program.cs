﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleship
{
    class Program
    {
        static void Main(string[] args)
        {
            // Setup
            uint maxShips = 0,
                players = 0;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("SETUP");
                try
                {
                    Console.Write("Maximum ships for a player: ");
                    maxShips = Convert.ToUInt32(Console.ReadLine());
                    if(maxShips < 1)
                    {
                        Console.WriteLine("Minimum number of ships is 1.");
                        Console.ReadKey();
                        continue;
                    }
                    Console.Write("Number of players: ");
                    players = Convert.ToUInt32(Console.ReadLine());
                    if(players < 2)
                    {
                        Console.WriteLine("Minimum number of players is 2.");
                        Console.ReadKey();
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    if (ex is FormatException || ex is OverflowException)
                    {
                        Console.WriteLine("Invalid format!");
                        Console.ReadKey();
                        continue;
                    }
                }
                break;
            }
            Board[] b = new Board[players];
            Player[] p = new Player[players];
            for(int i = 0; i < players; i++)
            {
                try
                {
                    Console.Clear();
                    Console.WriteLine("SETUP");
                    Console.Write($"Player {i + 1} name (optional): ");
                    string name = Console.ReadLine();
                    if (name == "") name = "PLAYER " + (i + 1);
                    Console.Write($"{name}'s board size (optional): ");
                    uint size = 10;
                    string t_size = Console.ReadLine();
                    if (t_size != "") size = Convert.ToUInt32(t_size);
                    if(size < 5)
                    {
                        Console.WriteLine("Minimum size of a board is 5.");
                        i--;
                        Console.ReadKey();
                    }
                    b[i] = new Board((int)size);
                    p[i] = new Player(name, b[i]);
                }
                catch(Exception ex)
                {
                    if(ex is FormatException || ex is OverflowException)
                    {
                        Console.WriteLine("Invalid format!");
                        i--;
                        Console.ReadKey();
                    }
                }
            }

            // Creating Ships
            for (int i = 0; i < p.Length; i++)
            {
                Console.Clear();
                Console.WriteLine(p[i].name);
                for (int j = 0; j < maxShips; j++)
                {
                    try
                    {
                        Console.Clear();
                        p[i].Show();
                        Console.WriteLine($"SHIP {j + 1} of {maxShips}");
                        Console.Write("Coordinate: ");
                        uint[] pos = Array.ConvertAll(Console.ReadLine().Split(' '), UInt32.Parse);
                        if(pos.Length != 2)
                        {
                            Console.WriteLine("Invalid format!");
                            j--;
                            Console.ReadKey();
                            continue;
                        }
                        Console.Write("Size: ");
                        uint size = Convert.ToUInt32(Console.ReadLine());
                        Orientation orient = Orientation.HORIZONTAL;
                        if (size > 1)
                        {
                            Console.Write("Orientation (0 for VERTICAL, 1 for HORIZONTAL): ");
                            string ori = Console.ReadLine();
                            orient = ori == "0" || ori == "VERTICAL" ? Orientation.VERTICAL : Orientation.HORIZONTAL;
                            if (ori == "0" || ori == "VERTICAL") orient = Orientation.VERTICAL;
                            else if (ori == "1" || ori == "HORIZONTAL") orient = Orientation.HORIZONTAL;
                            else
                            {
                                Console.WriteLine("Invalid format!");
                                j--;
                                Console.ReadKey();
                                continue;
                            }
                        }
                        try
                        {
                            b[i].AddShip(new Ship((int)pos[0] - 1, (int)pos[1] - 1, (int)size, orient, b[i]));
                        }
                        catch (FieldAlreadyHasShip)
                        {
                            Console.WriteLine("The coordinates you chose already contains a part of a ship.");
                            j--;
                            Console.ReadKey();
                        }
                        catch (InvalidShip)
                        {
                            Console.WriteLine("Your ship is out of boundaries.");
                            j--;
                            Console.ReadKey();
                        }
                    }
                    catch(Exception ex)
                    {
                        if (ex is FormatException || ex is OverflowException)
                        {
                            Console.WriteLine("Invalid format!");
                            j--;
                            Console.ReadKey();
                        }
                    }
                }
            }

            // Game cycle
            while (b.Where(x => x.ships.Count > 0).ToArray().Length > 1)
            {
                for (int i = 0; i < p.Length; i++)
                {
                    if (p[i].board.ships.Count < 1) continue;
                    try
                    {
                        Console.Clear();
                        p[i].Show();
                        Console.WriteLine($"{p[i].name} is attacking");
                        List<int> playerids = new List<int>();
                        foreach (Player player in p)
                        {
                            if (player.id != p[i].id && player.board.ships.Count > 0) playerids.Add(player.id);
                        }
                        Console.Write($"Player ID ({String.Join(", ", playerids)}): ");
                        uint id = Convert.ToUInt32(Console.ReadLine());
                        if (!playerids.Contains((int)id))
                        {
                            Console.WriteLine("Thats't not a valid Player ID.");
                            i--;
                            Console.ReadKey();
                            continue;
                        }
                        Console.Write("Coordinate: ");
                        uint[] pos = Array.ConvertAll(Console.ReadLine().Split(' '), UInt32.Parse);
                        if(pos.Length != 2)
                        {
                            Console.WriteLine("Invalid format!");
                            i--;
                            continue;
                        }
                        p[i].Attack(p[id - 1], (int)pos[0] - 1, (int)pos[1] - 1);

                        if (b.Where(x => x.ships.Count > 0).ToArray().Length < 2)
                        {
                            Console.Clear();
                            Console.WriteLine($"The winner is: {p.First(x => x.board.ships.Count > 0).name}");
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex is FormatException || ex is OverflowException)
                        {
                            Console.WriteLine("Invalid format!");
                            i--;
                            Console.ReadKey();
                        }
                    }
                }
            }
        }
    }

    enum Orientation
    {
        VERTICAL,
        HORIZONTAL
    }

    enum FieldState
    {
        NEUTRAL,
        HIT,
        MISS
    }

    class Field
    {
        public int x, y;
        public FieldState state = FieldState.NEUTRAL;

        public Field(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override bool Equals(object obj)
        {
            Field field = (Field)obj;
            return field.x == x && field.y == y;
        }

        public ConsoleColor GetState()
        {
            switch(state)
            {
                case FieldState.HIT: return ConsoleColor.Red;
                case FieldState.MISS: return ConsoleColor.Blue;
                default: return ConsoleColor.Black;
            }
        }
    }

    class Board
    {
        public List<Field> fields = new List<Field>();
        public List<Ship> ships = new List<Ship>();
        public int size;

        public Board(int size)
        {
            this.size = size;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    fields.Add(new Field(i, j));
                }
            }
        }

        public void AddShip(Ship ship)
        {
            if (ships.Any(s => s.body.Any(b => ship.body.Any(x => b.Equals(x))))) throw new FieldAlreadyHasShip();
            ships.Add(ship);
        }

        public void RemoveShip(Ship ship)
        {
            ships.Remove(ship);
        }
    }

    class Player
    {
        static int players = 0;
        public int id;
        public string name;
        public Board board;

        public Player(string name, Board board)
        {
            this.name = name;
            this.board = board;
            players++;
            id = players;
        }

        public void Attack(Player player, int x, int y)
        {
            Field field = new Field(x, y);
            if (player.board.ships.Any(s => s.body.Any(b => b.Equals(field))))
            {
                Ship ship = player.board.ships.First(s => s.body.Any(b => b.Equals(field)));
                ship.body.Remove(field);
                if (ship.body.Count > 0)
                {
                    // talált
                    board.fields[x * board.size + y].state = FieldState.HIT;
                    Console.WriteLine("You have hit a ship!");
                    Console.ReadKey();
                }
                else
                {
                    // hajó elsüllyedt
                    player.board.RemoveShip(ship);
                    board.fields[x * board.size + y].state = FieldState.HIT;
                    Console.WriteLine("You have sunk a ship!");
                    if (player.board.ships.Count < 1) Console.WriteLine($"{player.name} is out!"); // játékos kiesett
                    Console.ReadKey();
                }
            }
            else
            {
                // nem talált
                board.fields[x * board.size + y].state = FieldState.MISS;
                Console.WriteLine("You haven't hit any ship!");
                Console.ReadKey();
            }
        }

        public void Show()
        {
            Console.WriteLine($"{name}'s board");
            for (int i = 0; i < board.size; i++)
            {
                Console.Write($"{i + 1}.\t");
                for (int j = 0; j < board.size; j++)
                {
                    Console.BackgroundColor = board.fields[i * board.size + j].GetState();
                    Console.Write(board.ships.Any(x => x.body.Contains(board.fields[i * board.size + j])) ?
                        "[X]" : "[ ]");
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                Console.WriteLine();
            }
            Console.Write("\t");
            for (int i = 0; i < board.size; i++) Console.Write($"{i + 1}. ");
            Console.WriteLine();
        }
    }

    class Ship
    {
        public List<Field> body = new List<Field>();
        public Board board;

        public Ship(int x, int y, int size, Orientation orientation, Board board)
        {
            this.board = board;
            if ((orientation == Orientation.VERTICAL && (x + size < 0 || x + size > this.board.size)) ||
                (orientation == Orientation.HORIZONTAL && (y + size < 0 || y + size > this.board.size)))
                throw new InvalidShip();
            for (int i = 0; i < size; i++)
            {
                if (orientation == Orientation.HORIZONTAL) body.Add(new Field(x, y + i));
                else body.Add(new Field(x + i, y));
            }
        }
    }

    class FieldAlreadyHasShip : Exception { }

    class InvalidShip : Exception { }
}